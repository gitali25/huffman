#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <limits.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

const int ASCII_SET_SIZE = 256;
unsigned long i;

void error(char* msg)
{
    printf("\nError : %s\n", msg);
    exit(-1);
}

unsigned long bytes_to_bits(unsigned long bytes)
{
    return bytes*8;
}

unsigned long bits_to_bytes(unsigned long bits)
{
    unsigned long bytes = bits/8;
    bytes += (bits%8)? 1 : 0;
    return bytes;
}

//-----------------------------------------------------------------
// Utils

struct Buffer {
    unsigned char* buffer;
    unsigned long index;
    unsigned long read_index;
    unsigned long size;
};

typedef struct Buffer Buffer;


Buffer* new_buffer(unsigned long initialSize)
{
    Buffer* buffer = (Buffer*) malloc(sizeof(Buffer));
    buffer->buffer = (unsigned char*) malloc(initialSize * sizeof(unsigned char));
    buffer->index = 0;
    buffer->read_index = 0;
    buffer->size = initialSize;
}

void copy(unsigned char *from, unsigned char *to, unsigned long size)
{
    unsigned char *end = from + size;
    for( ; from < end ; ++from, ++to) *to = *from;
}

Buffer* clone_buffer(Buffer* buffer)
{
    Buffer* cloned = new_buffer(buffer->size);
    cloned->index = buffer->index;
    cloned->read_index = buffer->read_index;
    copy(buffer->buffer, cloned->buffer, buffer->index);
    return cloned;
}

void append(unsigned char ch, Buffer *buffer)
{
    if(buffer->index == buffer->size) {
        unsigned char* biggerBuffer = (unsigned char*) malloc(buffer->size * 2 * sizeof(unsigned char));
        copy(buffer->buffer, biggerBuffer, buffer->size);
        buffer->size *= 2;
        free(buffer->buffer);
        buffer->buffer = biggerBuffer;
    }
    buffer->buffer[buffer->index] = ch;
    ++buffer->index;
}

unsigned char read_byte(Buffer *buffer)
{
    return buffer->buffer[buffer->read_index++];
}

void print_buffer(Buffer *buffer)
{
    int i = 0;
    for ( ; i < buffer->index ; ++i)
        printf("index %d -> %c [%d]\n", i, buffer->buffer[i], buffer->buffer[i]);
    printf("--------------------\n");
}

void print_buffer2(Buffer *buffer)
{
    for (i = 0 ; i < buffer->index ; ++i)
        printf("%d", buffer->buffer[i]);
}

void deinit_buffer(Buffer *buffer)
{
    free(buffer->buffer);
    free(buffer);
}

//------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------

struct BITFILE {
    FILE* file;
    Buffer *buffer;
    unsigned char temp_buffer;
    unsigned long bits;
    unsigned long bits_in_buffer;
};

typedef struct BITFILE BITFILE;

BITFILE* init_bfile(char* fileName, char* mode)
{
    BITFILE* bfile = (BITFILE *) malloc(sizeof(BITFILE));
    if(fileName != NULL && mode != NULL) {
        bfile->file = fopen(fileName, mode);
        if(!bfile->file) error("init_bfile() : Invalid file");
    } else {
        bfile->file = NULL;
    }
    bfile->temp_buffer = 0;
    bfile->buffer = NULL;
    bfile->bits = 0;
    bfile->bits_in_buffer = 0;
    return bfile;
}

unsigned long init_buffer(BITFILE *bfile)
{
    unsigned long size = bits_to_bytes(bfile->bits);
    bfile->buffer = new_buffer(size);
    return size;
}

void set_file_size_bits(BITFILE *bfile, unsigned long bits)
{
    bfile->bits = bits;
    init_buffer(bfile);
}

void deinit_bfile(BITFILE* bfile)
{
    if(bfile->file != NULL) fclose(bfile->file);
    deinit_buffer(bfile->buffer);
    free(bfile);
}

int read_bit(BITFILE* bfile)
{
    if(bfile->bits_in_buffer == bfile->bits) return EOF;
    if(!(bfile->bits_in_buffer % 8)) bfile->temp_buffer = read_byte(bfile->buffer);
    unsigned char temp = bfile->temp_buffer;

    bfile->temp_buffer = bfile->temp_buffer<<1;
    ++bfile->bits_in_buffer;
    return (temp & 128) == 128;
}

unsigned char read_byte_bfile(BITFILE* bfile)
{
    char byte = 0;
    for(i = 0 ; i<8 ; ++i) {
        byte = byte<<1;
        byte |= (read_bit(bfile))? 1 : 0;
    }
    return byte;
}

void file_content_to_buffer(BITFILE *bfile)
{
    fscanf(bfile->file, "%lu", &bfile->bits);
    unsigned long size = init_buffer(bfile);

    if(' ' != getc(bfile->file)) {
        printf("Wrong file Format\n");
        exit(-1);
    }
    while(size--) append(getc(bfile->file), bfile->buffer);
}

void write_bit(BITFILE* bfile, unsigned short bit)
{
    bfile->temp_buffer = bfile->temp_buffer<<1;
    bfile->temp_buffer |= (bit)? 1 : 0;
    ++bfile->bits_in_buffer;

    if(!(bfile->bits_in_buffer % 8)) append(bfile->temp_buffer, bfile->buffer);
}

void write_byte_bfile(BITFILE* bfile, unsigned short byte)
{
    for(i = 0 ; i<8 ; ++i) {
        write_bit(bfile, (byte& 128) == 128);
        byte = byte<<1;
    }
}

void buffer_content_to_file(BITFILE *bfile)
{
    if(bfile->bits_in_buffer % 8) {
        bfile->temp_buffer = bfile->temp_buffer<<(8 - (bfile->bits_in_buffer % 8));
        append(bfile->temp_buffer, bfile->buffer);
    }
    bfile->buffer->read_index = 0;
    fprintf(bfile->file, "%lu ", bfile->bits_in_buffer);
    while(bfile->buffer->read_index < bfile->buffer->index)
        fputc(read_byte(bfile->buffer), bfile->file);
}

//------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------

struct Tree {
    unsigned char character;
    unsigned long char_frequency;
    struct Tree* right;
    struct Tree* left;
    Buffer* encoding;
    bool skip;
};

struct Initial_Trees {
    struct Tree* char_frequencies;
    unsigned long size;
};

typedef struct Initial_Trees Forest;
typedef struct Tree Tree;

Forest* new_forest(bool skip)
{
    Forest* trees = (Forest*) malloc(sizeof(Forest));
    trees->char_frequencies = (Tree*) malloc(ASCII_SET_SIZE * sizeof(Tree));
    trees->size = ASCII_SET_SIZE;

    for(i = 0 ; i<trees->size ; ++i) {
        trees->char_frequencies[i].char_frequency = 0;
        trees->char_frequencies[i].character = i;
        trees->char_frequencies[i].left = NULL;
        trees->char_frequencies[i].right = NULL;
        trees->char_frequencies[i].encoding = NULL;
        trees->char_frequencies[i].skip = skip;
    }
    return trees;
}

void deinit_trees(Forest* trees)
{
    for(i = 0 ; i<trees->size ; ++i)
        if(trees->char_frequencies[i].encoding)deinit_buffer(trees->char_frequencies[i].encoding); // TO GO BACK TO, some trees are not in the forest
    free(trees->char_frequencies);
    free(trees);
}

int get_lowest_frequency_index(Forest* trees)
{
    unsigned short index = 0;
    unsigned long min = INT_MAX;

    for( i = 0 ; i<ASCII_SET_SIZE ; ++i)
        if(trees->char_frequencies[i].skip == false && trees->char_frequencies[i].char_frequency < min) {
            index = i;
            min = trees->char_frequencies[i].char_frequency;
        }
    trees->char_frequencies[index].skip = true;
    return index;
}

Tree* new_tree(char ch, int freq, Tree* right, Tree* left)
{
    Tree* tree = (Tree*) malloc(sizeof(Tree));
    tree->character = ch;
    tree->char_frequency = freq;
    tree->right = right;
    tree->left = left;
    tree->skip = true;
    return tree;
}

void set_encodings_rec(Tree* tree, Buffer* buffer)
{
    if(tree->left != NULL && tree->right != NULL) {
        Buffer* cloned = clone_buffer(buffer);
        append(0, buffer);
        set_encodings_rec(tree->left, buffer);

        append(1, cloned);
        set_encodings_rec(tree->right, cloned);
    } else {
        tree->encoding = buffer;
    }
}

Tree* make_tree(Forest* trees)
{
    while(trees->size > 1) {
        unsigned int index = get_lowest_frequency_index(trees);
        unsigned int index2 = get_lowest_frequency_index(trees);

        trees->char_frequencies[index].right = new_tree(trees->char_frequencies[index].character,
                                               trees->char_frequencies[index].char_frequency, trees->char_frequencies[index].right, trees->char_frequencies[index].left);
        trees->char_frequencies[index].left = &trees->char_frequencies[index2];
        trees->char_frequencies[index].character = 0;
        trees->char_frequencies[index].skip = false;
        trees->char_frequencies[index].char_frequency += trees->char_frequencies[index2].char_frequency;
        --trees->size;
    }
    Tree* head = &trees->char_frequencies[get_lowest_frequency_index(trees)];
    set_encodings_rec(head, new_buffer(3));
    return head;
}

void inorder(Tree* tree)
{
    if(tree->left != NULL && tree->right != NULL) {
        printf("@");
        inorder(tree->left);
        inorder(tree->right);
    } else {
        printf("%c ", tree->character);
        print_buffer2(tree->encoding);
        printf("\n");
    }
}

void print_unused(Forest* trees)
{
    printf("\nprint_unused(Forest* trees): %lu\n", trees->size);
    for( i = 0 ; i<ASCII_SET_SIZE ; ++i) {
        if(trees->char_frequencies[i].skip == false)
            printf("%c - %lu : %lu\n", trees->char_frequencies[i].character, trees->char_frequencies[i].char_frequency, i);
    }
    printf("\n----------------------------\n");
}

unsigned long set_frequencies(char* file_name, Forest* trees)
{
    FILE* file = fopen(file_name, "r");
    unsigned long char_counter = 0;
    if(!file) {
        perror("get_character_frequencies() : could not open file");
        exit(-1);
    }

    int char_;
    while((char_ = fgetc(file)) != EOF) {
        ++char_counter;
        ++trees->char_frequencies[char_].char_frequency;
    }

    for(i = 0 ; i<ASCII_SET_SIZE ; ++i)
        if(trees->char_frequencies[i].char_frequency == 0) {
            trees->char_frequencies[i].skip = true;
            --trees->size;
        }
    fclose(file);
    return char_counter;
}

void arrange_forest_rec(Forest* forest, Tree* tree)
{
    if(tree->left != NULL && tree->right != NULL) {
        arrange_forest_rec(forest, tree->left);
        arrange_forest_rec(forest, tree->right);
    } else {
        forest->char_frequencies[tree->character].char_frequency = tree->char_frequency;
        forest->char_frequencies[tree->character].character = tree->character;
        forest->char_frequencies[tree->character].encoding = tree->encoding;    // shared encoding
        forest->char_frequencies[tree->character].skip = false;
        forest->size++;
    }
}

Forest* arrange_forest(Tree* tree)
{
    Forest* arranged_trees = new_forest(true);
    arranged_trees->size = 0;
    arrange_forest_rec(arranged_trees, tree);
    return arranged_trees;
}

void encode(char* file_name, Forest* forest, BITFILE* bit_file)
{
    FILE* in_file = fopen(file_name, "r");
    int ch;
    while((ch = fgetc(in_file)) != EOF) {
        Tree* tree_ref = &forest->char_frequencies[ch];
        for(tree_ref->encoding->read_index = 0; tree_ref->encoding->read_index < tree_ref->encoding->index; )
            write_bit(bit_file, read_byte(tree_ref->encoding));
    }
    fclose(in_file);
}

void decode(char* file_name, Tree* tree, BITFILE* bit_file)
{
    FILE* out_file = fopen(file_name, "w");
    Tree* ptr = tree;
    int bit;
    while((bit = read_bit(bit_file)) != EOF) {
        if(bit == 1) ptr = ptr->right;
        else if(bit == 0) ptr = ptr->left;
        if(ptr->left == NULL && ptr->right == NULL) {
            fputc(ptr->character, out_file);
            ptr = tree;
        }
    }
    fclose(out_file);
}

void tree_size_rec(Tree *tree, unsigned long *bitSize)
{
    if(tree->left != NULL && tree->right != NULL) {
        *bitSize += 1;
        tree_size_rec(tree->left, bitSize);
        tree_size_rec(tree->right, bitSize);
    } else *bitSize += 9;
}

unsigned long tree_size(Tree* tree)
{
    unsigned long size = 0;
    tree_size_rec(tree, &size);
    return size;
}

void encode_tree(Tree* tree, BITFILE* bit_file)
{
    if(tree->left != NULL && tree->right != NULL) {
        write_bit(bit_file, 1);
        encode_tree(tree->left, bit_file);
        encode_tree(tree->right, bit_file);
    } else {
        write_bit(bit_file, 0);
        write_byte_bfile(bit_file, tree->character);
    }
}

Tree* decode_tree(BITFILE* bit_file)
{
    int bit = read_bit(bit_file);
    if(bit == EOF) return NULL;

    Tree* t = (Tree*) malloc(sizeof(Tree));
    if(bit == 1) {
        t->left = decode_tree(bit_file);
        t->right = decode_tree(bit_file);
    } else {
        t->character = read_byte_bfile(bit_file);
        t->left = NULL;
        t->right = NULL;
    }
    return t;
}

void combine(BITFILE* tree, BITFILE* file)
{
    buffer_content_to_file(file);
    fprintf(file->file, "%c", ' ');
    tree->file = file->file;
    buffer_content_to_file(tree);
    tree->file = NULL;
}

void uncombine(BITFILE* tree, BITFILE* file)
{
    file_content_to_buffer(file);
    if(' ' != getc(file->file)) error("Wrong file Format");
    tree->file = file->file;
    file_content_to_buffer(tree);
    tree->file = NULL;

}

int is_regular_file(const char *path)
{
    struct stat path_stat;
    stat(path, &path_stat);
    return S_ISREG(path_stat.st_mode);
}

int main(int args, char** argv)
{
    Forest* trees = new_forest(false);
    char* operation = argv[1];
    char* input_file_name = argv[2];
    char* output_file_name = argv[3];

    if(!is_regular_file(input_file_name)) error("directories cannot be hanled");

    if(operation[1]=='c') {
        unsigned long char_count = set_frequencies(input_file_name, trees);
        Tree* head = make_tree(trees);
        Forest* arranged_trees = arrange_forest(head);
        BITFILE* encoded_bit_file = init_bfile(output_file_name, "w");
        set_file_size_bits(encoded_bit_file, bytes_to_bits(char_count));
        encode(input_file_name, arranged_trees, encoded_bit_file);

        BITFILE *tree_file = init_bfile(NULL, NULL);
        set_file_size_bits(tree_file, tree_size(head));
        encode_tree(head, tree_file);

        combine(tree_file, encoded_bit_file);
        deinit_trees(arranged_trees);

    } else if(operation[1]=='d') {
        BITFILE *tree_file = init_bfile(NULL, NULL);
        BITFILE* decoded_bit_file = init_bfile(input_file_name, "r");
        uncombine(tree_file, decoded_bit_file);

        Tree* head = decode_tree(tree_file);
        decode(output_file_name, head, decoded_bit_file);

        deinit_bfile(decoded_bit_file);
        deinit_bfile(tree_file);
    } else error("bad argument : operation not supported");

    deinit_trees(trees);

    return 0;
}
